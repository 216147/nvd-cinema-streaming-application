import React, { ReactNode, useState } from 'react';
import { useMutation } from '@apollo/client';
import gql from 'graphql-tag';
import { useRouter } from 'next/router';

const RESET_PASSWORD_MUTATION = gql`
  mutation ResetPassword($token: String!, $newPassword: String!) {
    resetPassword(token: $token, newPassword: $newPassword) {
      success
      message
    }
  }
`;

const ResetPassword = () => {
  const [newPassword, setNewPassword] = useState('');
  const [resetPassword] = useMutation(RESET_PASSWORD_MUTATION);
  const router = useRouter();
  const { token } = router.query;

  const handleSubmit = async (e:React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { data } = await resetPassword({ variables: { token, newPassword } });

    if (data.resetPassword.success) {
      alert('Password reset successfully');
      router.push('/login')
    } else {
      alert(data.resetPassword.message);
    }
  };

  return (
    <form onSubmit={handleSubmit} className='max-w-sm mx-auto'>
      <input
        type="password"
        value={newPassword}
        onChange={(e) => setNewPassword(e.target.value)}
        placeholder="Enter new password"
        className='bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-0 dark:focus:border-gray-500 dark:focus:outline-none dark:focus:outline-gray-700'
        required
      />
      <button type="submit" className='text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-gray-500 dark:hover:bg-gray-600 dark:focus:ring-0 dark:focus:border-gray-500 dark:focus:outline-none dark:focus:outline-gray-700 mt-5'>Reset Password</button>
    </form>
  );
};

export default ResetPassword;
